package model;

import interfaces.Movable;

public class Movablepoint implements Movable {
    private int x;
    private int y;
    public Movablepoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    @Override
    public String toString() {
        return "Movablepoint [x=" + x + ", y=" + y + "]";
    }
    @Override
    public void moveUp(){
        this.y++;
    }
    @Override
    public void moveDown(){
        this.y--;
    }
    @Override
    public void moveLeft(){
        this.x--;
    }
    @Override
    public void moveRight(){
        this.x++;
    }
}
